// #include "Array.hpp"
// #include <iostream>
// #include <string>

// int main()
// {
// 	Array<int> def = Array<int>();
// 	Array<std::string> normal = Array<std::string>(5);
// 	std::string word = "this is bruh momento";
// 	std::cout << "Size of an empty Array = " << def.size() << std::endl;
// 	try
// 	{
// 		def[0];
// 	}
// 	catch (std::exception &)
// 	{
// 		std::cout << "Could not access 0-th element of empty Array\n" << std::endl;
// 	}
// 	std::cout << "Size of an Array(5) = " << normal.size() << std::endl;
// 	for (int i = 0; i < normal.size(); ++i)
// 	{
// 		word[i] = 'A' + i;
// 		normal[i] = word;
// 	}
// 	for (int i = 0; i < normal.size() + 1; ++i)
// 		std::cout << normal[i] << std::endl;
// }

#include <iostream>
#include <cstdlib>
#include "Array.hpp"

#define MAX_VAL 750
int main(int, char**)
{
    Array<int> numbers(MAX_VAL);
    int* mirror = new int[MAX_VAL];
    srand(time(NULL));
    for (int i = 0; i < MAX_VAL; i++)
    {
        const int value = rand();
        numbers[i] = value;
        mirror[i] = value;
    }
    //SCOPE
    {
        Array<int> tmp = numbers;
        Array<int> test(tmp);
    }

    for (int i = 0; i < MAX_VAL; i++)
    {
        if (mirror[i] != numbers[i])
        {
            std::cerr << "didn't save the same value!!" << std::endl;
            return 1;
        }
    }
    try
    {
        numbers[-2] = 0;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    try
    {
        numbers[MAX_VAL] = 0;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    for (int i = 0; i < MAX_VAL; i++)
    {
        numbers[i] = rand();
    }
    delete [] mirror;//
    return 0;
}