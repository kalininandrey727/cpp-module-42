#pragma once
#include <exception>

template <typename T>
class Array
{
public:
	Array() : data(new T[0]), _size(0) {}
	Array(int new_size) : data(new T[new_size]), _size(new_size) {}
	Array(const Array<T>& rhs) : data(new T[rhs.size()]), _size(rhs.size())
	{
		for (int i = 0; i != rhs.size(); ++i)
			this->data[i] = rhs.getData()[i];
	}
	Array<T>& operator=(const Array<T>& rhs)
	{
		data = new T[rhs.size()];
		_size = rhs.size();
		for (int i = 0; i != rhs.size(); ++i)
			this->data[i] = rhs.getData()[i];
	}
	T& operator[](int idx)
	{
		if (!(idx < 0 || idx >= _size))
		 	return data[idx];
		else
			throw std::exception();
	}
	int size() const   { return _size; }
	T* getData() const { return data; }

private:
	T *data;
	int _size;
};
