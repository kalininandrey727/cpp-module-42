template<typename T>
void swap(T& lhs, T& rhs)
{
	T buf = lhs;
	lhs = rhs;
	rhs = buf;
}

template<typename T>
T min(const T& lhs, const T& rhs)
{
	if (lhs > rhs)
		return rhs;
	return lhs;
}

template<typename T>
T max(const T& lhs, T rhs)
{
	if (lhs > rhs)
		return lhs;
	return rhs;
}