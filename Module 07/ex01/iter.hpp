template <typename T>
void iter(T* arr, int len, void (*func)(T&))
{
	for (int i = 0; i < len; ++i)
		func(arr[i]);
}

template <typename T>
void increment(T& arg)
{
	++arg;
}

template <typename T>
void change_str_0_to_A(T& arg)
{
	arg[0] = 'A';
}