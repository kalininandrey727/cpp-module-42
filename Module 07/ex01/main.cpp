#include "iter.hpp"
#include <iostream>

int main()
{
	const int size = 5;
	int 		*arr = new int[size];
	std::string *names = new std::string[size];

	for (int i = 0; i < size; ++i)
		arr[i] = i;
	names[0] = "Andrey";
	names[1] = "Bob";
	names[2] = "Carl";
	names[3] = "Deez";
	names[4] = "123";
	
	iter(arr, size, increment);
	for (int i = 0; i < size; ++i)
		std::cout << arr[i] << std::endl;
	std::cout << std::endl;
	iter(names, size, change_str_0_to_A);
	for (int i = 0; i < size; ++i)
		std::cout << names[i] << std::endl;
}