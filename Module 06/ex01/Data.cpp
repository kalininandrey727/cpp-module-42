#include "Data.hpp"

Data::Data() : lol(727) {}

Data::Data(int Nibelungen) : lol(Nibelungen) {}

Data::Data(const Data& Nibelungen) : lol(Nibelungen.lol) {}

Data::~Data() {}

Data &Data::operator=(const Data &Nibelungen)
{
	this->lol = Nibelungen.lol;
	return *this;
}

uintptr_t serialize(Data* ptr)
{
	return (reinterpret_cast<uintptr_t>(ptr));
}

Data* deserialize(uintptr_t raw)
{
	return (reinterpret_cast<Data *>(raw));
}
