#include <iostream>
#include <iomanip>
#include <sstream>
#include <ctype.h>
#include <cmath>
#include <stdint.h>

class NumberConverter
{
private:
	enum Types {
		INVALID,
		CHAR,
		INT,
		FLOAT,
		DOUBLE,
		PSEUDO
	};

	const std::string arg;
	int type;

	int64_t	intValue;
	double	doubleValue;

	bool charConvertable();
	bool isInteger();
	bool isFloat();
	bool isPseudo();
	void getType();
	void convertToNum();
	void usage();

	void printChar();
	void printInt();
	void printFloat();
	void printDouble();
public:
	NumberConverter();
	NumberConverter(const char *argument);
	NumberConverter(const std::string argument);
	NumberConverter(const NumberConverter &rhs);
	~NumberConverter();
	NumberConverter &operator=(const NumberConverter &rhs);
	void printInfo();
};