#include "NumberConverter.hpp"

NumberConverter::NumberConverter() : arg("")
{

}

NumberConverter::NumberConverter(const char *argument) : arg(argument)
{
	getType();
	convertToNum();
	// printInfo();
}

NumberConverter::NumberConverter(const std::string argument) : arg(argument)
{

}

NumberConverter::NumberConverter(const NumberConverter &rhs) : arg("")
{
	intValue = rhs.intValue;
	doubleValue = rhs.doubleValue;

}

NumberConverter::~NumberConverter() {}

NumberConverter &NumberConverter::operator=(const NumberConverter &rhs)
{
	type = rhs.type;
	intValue = rhs.intValue;
	doubleValue = rhs.doubleValue;
	return *this;
}

bool NumberConverter::isFloat()
{
    std::string::const_iterator	it = arg.begin();
	int							dotcount = 0;

    if (it != arg.end() && (*it == '-' || *it == '+'))
		++it;
    while (it != arg.end() && dotcount != 2)
	{
		if (!std::isdigit(*it))
		{
			if (*it == '.')
				++dotcount;
			else
			{
				if (*it == 'f')
					++it;
			 	break;
			}
		}
		++it;
	}
	return !arg.empty() && it == arg.end() && dotcount != 2;
}

bool NumberConverter::isPseudo()
{
	std::string pseudo_literals[] = {
		"-inf",
		"+inf",
		"nan",
		"-inff",
		"+inff",
		"nanf",
	};
	for (std::string *p = pseudo_literals; p != pseudo_literals + 6; ++p)
		if (arg == *p)
			return true;
	return false;
}

bool NumberConverter::isInteger()
{
    std::string::const_iterator it = arg.begin();
    if (it != arg.end() && (*it == '-' || *it == '+'))
		++it;
    while (it != arg.end() && std::isdigit(*it))
		++it;
    return !arg.empty() && it == arg.end();
}

void NumberConverter::getType()
{
	if (isPseudo())
		type = PSEUDO;
	else if (isInteger())
		type = INT;
	else if (arg.size() == 1)
		type = CHAR;
	else if (isFloat())
	{
		if (*(arg.end() - 1) == 'f')
			type = FLOAT;
		else if (std::isdigit(*(arg.end() - 1)) || *(arg.end() - 1) == '.')
			type = DOUBLE;
		else
			type = INVALID;
	}
	else
	 	type = INVALID;
}

void NumberConverter::convertToNum()
{
	std::stringstream ss;

	if (type == CHAR)
		ss << static_cast<int>(arg[0]);
	else if (type != INVALID)
		ss << arg;
	ss >> intValue;

	ss.clear();
	ss.str(std::string());

	if (type == CHAR)
		ss << static_cast<int>(arg[0]);
	else if (type != INVALID)
		ss << arg;
	ss >> doubleValue;
}

void NumberConverter::usage()
{
	std::cout << "Usage: ./convert {number}\n";
}

void NumberConverter::printChar()
{
	std::cout << "char: ";
	if (type == PSEUDO || !(0 <= intValue && intValue <= 127))
		std::cout << "Impossible";
	else if (!std::isprint(static_cast<char>(intValue)))
		std::cout << "Non-displayable";
	else
		std::cout << "'" << static_cast<char>(intValue) << "'";
	std::cout << std::endl;
}

void NumberConverter::printInt()
{
	std::cout << "int: ";
	if (type == PSEUDO || !(INT32_MIN <= intValue && intValue <= INT32_MAX))
		std::cout << "Impossible";
	else
		std::cout << intValue;
	std::cout << std::endl;
}

void NumberConverter::printFloat()
{
	std::cout << "float: ";
	if (type == PSEUDO)
	{
		std::cout << arg;
		if (*(arg.end() - 1) != 'f' || *(arg.end() - 3) == 'i')
			std::cout << 'f';
	}
	else
	{
		float floatValue = static_cast<float>(doubleValue);
		if (std::floor(floatValue) == floatValue)
			std::cout << std::fixed << std::setprecision(1) << floatValue << "f";
		else
			std::cout << std::setprecision(7) << floatValue << "f";
	}
	std::cout << std::endl;
}

void NumberConverter::printDouble()
{
	std::cout << "double: ";
	if (type == PSEUDO)
	{
		for (std::string::const_iterator it = arg.begin(); it != arg.end() - 1; ++it)
			std::cout << *it;
		if (*(arg.end() - 1) != 'f' || *(arg.end() - 3) == 'i')					// i dont know why does it work
			std::cout << *(arg.end() - 1);
	}
	else if (std::floor(doubleValue) == doubleValue)
		std::cout << std::fixed << std::setprecision(1) << doubleValue;
	else
		std::cout << std::setprecision(16) << doubleValue;
	std::cout << std::endl;
}

void NumberConverter::printInfo()
{
	if (type == INVALID)
		usage();
	else
	{
		printChar();
		printInt();
		printFloat();
		printDouble();
	}
	
}