#include "A.hpp"
#include "B.hpp"
#include "C.hpp"
#include <typeinfo>
#include <iostream>

#define nullptr NULL

Base* generate()
{
	static int rng = 0;
	++rng;
	if (rng % 3 == 1)
		return new A();
	if (rng % 3 == 2)
		return new B();
	return new C();
}

void identify(Base* p)
{
	Base *converted;
	converted = dynamic_cast<A*>(p);
	if (converted != nullptr)
		std::cout << "A\n";
	else
	{
		converted = dynamic_cast<B*>(p);
		if (converted != nullptr)
			std::cout << "B\n";
		else
		{
			converted = dynamic_cast<C*>(p);
			if (converted != nullptr)
				std::cout << "C\n";
			else
				std::cout << "Base\n";	
		}
	}
}

void identify(Base& p)
{
	Base converted;
	try
	{
		converted = dynamic_cast<A&>(p);
		std::cout << "A\n";
	} 
	catch (const std::bad_cast& e)
	{
		try
		{
			converted = dynamic_cast<B&>(p);
			std::cout << "B\n";
		}
		catch (const std::bad_cast& e)
		{
			try
			{
				converted = dynamic_cast<C&>(p);
				std::cout << "C\n";
			}
			catch (const std::bad_cast& e)
			{
				std::cout << "Base\n";	
			}
		}
	}
}

int main()
{
	for (int i = 0; i < 6; ++i)
	{
		Base *ptr = generate();
		identify(ptr);
		delete ptr;
	}
	for (int i = 0; i < 6; ++i)
	{
		Base *ptr = generate();
		identify(*ptr);
		delete ptr;
	}
}
