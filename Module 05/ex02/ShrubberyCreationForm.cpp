#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(const std::string &target) :
	Form("ShrubberyCreationForm", 145, 137), _target(target) {}

ShrubberyCreationForm::ShrubberyCreationForm(void) :
	Form("ShrubberyCreationForm", 145, 137), _target("") {}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const & src):
	Form("ShrubberyCreationForm", 145, 137), _target(src.getTarget())
{
	*this = src;
}

ShrubberyCreationForm::~ShrubberyCreationForm(void) {}

ShrubberyCreationForm &	ShrubberyCreationForm::operator=(ShrubberyCreationForm const & rhs)
{
	rhs.getTarget();
	return *this;
}

const std::string	&ShrubberyCreationForm::getTarget(void) const
{
	return this->_target;
}

void	ShrubberyCreationForm::beExecuted(const Bureaucrat &executor) const
{
	std::ofstream	file;
	std::string		fileName = this->_target + "_shrubbery";

	this->tryToExecute(executor);
	file.open(fileName.c_str(), std::ios::out);
	file	<< "888\n"
			<< "888\n" 
			<< "888\n"  
			<< "888888 888d888 .d88b.   .d88b.\n"  
			<< "888    888P*  d8P  Y8b d8P  Y8b\n"
			<< "888    888    88888888 88888888\n"
			<< "Y88b.  888    Y8b.     Y8b.    \n"
			<< " *Y888 888     *Y8888   *Y8888  *`" << std::endl;
	file.close();
	std::cout << "Created the tree in " << this->_target << "_shrubbery" << std::endl;
}
