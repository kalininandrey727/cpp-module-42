#pragma once
#include <iostream>
#include "Form.hpp"
#include "Bureaucrat.hpp"

class PresidentialPardonForm : public Form
{
public:
	PresidentialPardonForm(const std::string &target);
	PresidentialPardonForm(const PresidentialPardonForm &src);
	~PresidentialPardonForm(void);
	PresidentialPardonForm	&operator=(const PresidentialPardonForm &rhs);

	const std::string	&getTarget(void) const;
	virtual void		beExecuted(const Bureaucrat &executor) const;

private:
	const std::string	_target;
	PresidentialPardonForm(void);
};
