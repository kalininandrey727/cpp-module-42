#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(const std::string &target):
	Form("RobotomyRequestForm", 72, 45), _target(target) {}

RobotomyRequestForm::RobotomyRequestForm(void):
	Form("RobotomyRequestForm", 72, 45), _target("") {}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const & src):
	Form("RobotomyRequestForm", 72, 45), _target(src.getTarget())
{
	*this = src;
}

RobotomyRequestForm::~RobotomyRequestForm(void) {}

RobotomyRequestForm&	RobotomyRequestForm::operator=(RobotomyRequestForm const & rhs)
{
	rhs.getTarget();
	return *this;
}

const std::string&	RobotomyRequestForm::getTarget(void) const
{
	return this->_target;
}

void	RobotomyRequestForm::beExecuted(const Bureaucrat &executor) const
{
	this->tryToSign(executor);
	std::cout << "* ЖЖЖЖЪЖЖЪЖЖЖЪ БЖЖЖЖЪ *" << std::endl;
	if (std::rand() % 2)
		std::cout << this->_target << " has been successfully transformed into a cyborg!" << std::endl;
	else
		std::cout << this->_target << " may have lost a limb or two... " << std::endl;
}
