#include "Bureaucrat.hpp"
#include <cstdlib>
#include <iostream>

int main()
{
	Bureaucrat first("amogus");
	Bureaucrat second("bog", 123);
	Bureaucrat evil("evil", 727);

	std::cout	<< first	<< std::endl
				<< second	<< std::endl;
	first.incrementGrade();
	std::cout << first << std::endl;
	first.decrementGrade();
	std::cout << first << std::endl;
	while (true)
	{
		second.incrementGrade();
		//std::cout << second << std::endl;
	}
}