#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(const std::string &target):
	Form("PresidentialPardonForm", 25, 5), _target(target) {}

// PresidentialPardonForm::PresidentialPardonForm(void):
// 	Form("PresidentialPardonForm", 25, 5), _target("") {}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const & src):
	Form("PresidentialPardonForm", 25, 5), _target(src.getTarget())
{
	*this = src;
}

PresidentialPardonForm::~PresidentialPardonForm(void) {}

PresidentialPardonForm &	PresidentialPardonForm::operator=(PresidentialPardonForm const & rhs)
{
	rhs.getTarget();
	return *this;
}

const std::string	&PresidentialPardonForm::getTarget(void) const
{
	return this->_target;
}

void	PresidentialPardonForm::beExecuted(const Bureaucrat &executor) const
{
	if (this->getSigned() == true && executor.getGrade() <= this->getGte())
		std::cout << this->_target << " has been pardoned by Zafod Beeblebrox." << std::endl;
}
