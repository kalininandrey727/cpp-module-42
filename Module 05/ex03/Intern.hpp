#include "Form.hpp"
#include "Bureaucrat.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"

class Intern
{
public:
	Intern();
	Intern(const Intern &src);
	~Intern();
	Intern &operator=(const Intern &rhs);

	Form *makeForm(std::string form_type, std::string form_target);

private:
	template<typename V1, typename V2>
	class Pair
	{
	public:
		V1 first;
		V2 second;
		Pair<V1, V2>() {}
		Pair<V1, V2>(const V1& f, const V2& s) : first(f), second(s) {}
		Pair<V1, V2>(const Pair<V1, V2>& other) : first(other.first), second(other.second) {}
		Pair<V1, V2>& operator=(const Pair<V1, V2>& other)
		{
			first = other.first;
			second = other.second;
			return *this;
		}
		~Pair<V1, V2>() {}
	};
	void amogus();

	Pair<std::string, Form *>* forms;
};