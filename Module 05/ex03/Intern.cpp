#include "Intern.hpp"

// template<typename V1, typename V2>
// Intern::Pair<V1, V2>::Pair(const V1& f, const V2& s) : first(f), second(s) {}

Intern::Intern() { amogus(); }
Intern::Intern(const Intern &src) { amogus(); (void)src; }
Intern::~Intern() {}
Intern &Intern::operator=(const Intern &rhs)
{
	(void)rhs;
	return *this;
}

Form *Intern::makeForm(std::string form_type, std::string form_target)
{
	// forms = new Pair<std::string, Form *>[3];
	Form *form = NULL;

	forms[0].second = new PresidentialPardonForm(form_target);
	forms[1].second = new RobotomyRequestForm(form_target);
	forms[2].second = new ShrubberyCreationForm(form_target);

	for (int i = 0; i < 3; ++i)
	{
		if (forms[i].first == form_type)
			form = forms[i].second;
		else
			delete forms[i].second;
	}
	return form;
}

void Intern::amogus()
{
	forms = new Pair<std::string, Form *>[3];

	forms[0].first = "presidential pardon";
	forms[0].second = NULL;
	forms[1].first = "robotomy request";
	forms[1].second = NULL;
	forms[2].first = "shrubbery creation";
	forms[2].second = NULL;
}