#pragma once
#include <iostream>
#include <fstream>
#include "Form.hpp"
#include "Bureaucrat.hpp"

class ShrubberyCreationForm : public Form
{
public:
	ShrubberyCreationForm(const std::string &target);
	ShrubberyCreationForm(const ShrubberyCreationForm &src);
	~ShrubberyCreationForm(void);
	ShrubberyCreationForm	&operator=(const ShrubberyCreationForm &rhs);

	const std::string	&getTarget(void) const;
	virtual void		beExecuted(const Bureaucrat &executor) const;

private:
	const std::string	_target;
	ShrubberyCreationForm(void);
};
