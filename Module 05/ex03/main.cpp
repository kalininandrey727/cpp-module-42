#include "Bureaucrat.hpp"
#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>

int	main(void)
{
	Intern		usefulIdiot;
	Bureaucrat	paul("Paul", 3);
	Form		*form;

	form = usefulIdiot.makeForm("this one doesn't exist", "Justin");
	if (form)
	{
		form->beSigned(paul);
		form->beExecuted(paul);
		delete form;
	}
	form = usefulIdiot.makeForm("presidential pardon", "Maynard");
	if (form)
	{
		form->beSigned(paul);
		form->beExecuted(paul);
		delete form;
	}
	form = usefulIdiot.makeForm("shrubbery creation", "Danny");
	if (form)
	{
		form->beSigned(paul);
		form->beExecuted(paul);
		delete form;
	}
	form = usefulIdiot.makeForm("robotomy request", "Adam");
	if (form)
	{
		form->beSigned(paul);
		form->beExecuted(paul);
		delete form;
	}
}