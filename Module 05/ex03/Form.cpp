#include "Form.hpp"

Form::Form(const std::string& name_, const int grade_to_sign, const int grade_to_execute) : name(name_), is_signed(false), gts(grade_to_sign), gte(grade_to_execute) 
{
	checkNewGrade(gts);
	checkNewGrade(gte);
}

Form::~Form() {}

Form::Form(const Form &rhs) : is_signed(rhs.is_signed), gts(rhs.gts), gte(rhs.gte) {}

Form& Form::operator=(const Form& rhs)
{
	is_signed = rhs.is_signed;
	return *this;
}

int Form::checkNewGrade(int new_grade)
{
	if (new_grade < 1)
		throw Form::GradeTooHighException(getInfo());
	if (new_grade > 150)
		throw Form::GradeTooLowException(getInfo());
	return new_grade;
}

void Form::tryToSign(const Bureaucrat &brc) const
{
	if (!is_signed && this->getGts() < brc.getGrade())
		throw Form::GradeTooLowException(brc.getExInfo() + " tried to sign " + this->getExInfo() + ", but his grade is too low");
}

void Form::tryToExecute(const Bureaucrat &brc) const
{
	if (!is_signed)
		throw Form::GradeTooLowException(brc.getExInfo() + " tried to execute " + this->getExInfo() + ", but form is not signed");
	if (this->getGte() < brc.getGrade())
		throw Form::GradeTooLowException(brc.getExInfo() + " tried to execute " + this->getExInfo() + ", but his grade is too low");
}

void Form::beSigned(const Bureaucrat &brc)
{
	if (!is_signed)
	{
		if (this->gts < brc.getGrade())
			throw Form::GradeTooLowException(brc.getExInfo() + " tried to sign " + this->getExInfo() + ", but his grade is too low");
		else
			is_signed = true;
	}
}

const std::string &	Form::getName()		const { return name; }
bool				Form::getSigned()	const { return is_signed; }
int					Form::getGts()		const { return gts; }
int					Form::getGte()		const { return gte; }

std::string Form::getInfo() const
{
	std::ostringstream os;
	os << "Form \'" << getName() << "\', is ";
	if (!getSigned()) os << "not ";
	os << "signed, grade to sign: " << getGts() << ", grade to execute: " << getGte();
	return os.str(); 
}

std::string Form::getExInfo() const
{
	std::ostringstream os;
	os << "form \'" << getName() << "\' (gts " << getGts() << ", gte " << getGte() << ")";
	return os.str(); 
}

std::ostream &operator<<(std::ostream &os, Form &form)
{
	return os << form.getInfo();
}

Form::GradeTooLowException::GradeTooLowException(const std::string &msg) throw() : message(msg) {}
Form::GradeTooLowException::~GradeTooLowException() throw() {}

const char * Form::GradeTooLowException::what() const throw()
{
	return this->message.c_str();
}

Form::GradeTooHighException::GradeTooHighException(const std::string &msg) throw() : message(msg) {}
Form::GradeTooHighException::~GradeTooHighException() throw() {}

const char * Form::GradeTooHighException::what() const throw()
{
	return this->message.c_str();
}