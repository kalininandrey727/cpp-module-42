#pragma once

#include "Bureaucrat.hpp"

class Form {
private:
	const std::string name;
	bool is_signed;
	const int gts;	
	const int gte;
	int checkNewGrade(int new_grade);
public:
	Form(const std::string& name_, const int grade_to_sign, const int grade_to_execute);
	~Form();
	Form(const Form &rhs);
	Form& operator=(const Form& rhs);

	void beSigned(const Bureaucrat &);
	const std::string &	getName()	const;
	bool				getSigned()	const;
	int					getGts()	const;
	int					getGte()	const;
	std::string 		getInfo()	const;
	std::string 		getExInfo()	const;

	class GradeTooLowException : public std::exception {
	private:
		std::string message;
	public:
		GradeTooLowException(const std::string &msg) throw();
		~GradeTooLowException() throw();
		const char * what() const throw();
	};

	class GradeTooHighException : public std::exception {
	private:
		std::string message;
	public:
		GradeTooHighException(const std::string &msg) throw();
		~GradeTooHighException() throw();
		const char * what() const throw();
	};
};

std::ostream &operator<<(std::ostream &os, Form &form);