#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <exception>

class Bureaucrat {
protected:
	const std::string name;
	int grade;
	int checkNewGrade(int new_grade);
public:
	Bureaucrat(const std::string &, int grade_ = 150);
	Bureaucrat(const Bureaucrat &);
	Bureaucrat &operator=(const Bureaucrat &);
	~Bureaucrat();

	const std::string 	&getName()	const;
	int					getGrade()	const;
	std::string			getInfo()	const;
	std::string			getExInfo()	const;

	void incrementGrade();
	void decrementGrade();

	class ExceptionGradeTooHigh : public std::exception {
	private:
		std::string message;
	public:
		ExceptionGradeTooHigh(const std::string &msg) throw();
		~ExceptionGradeTooHigh() throw();
		const char * what() const throw();// noexcept override;
	};
	class ExceptionGradeTooLow  : public std::exception {
	private:
		std::string message;
	public:
		ExceptionGradeTooLow(const std::string &msg) throw();
		~ExceptionGradeTooLow() throw();
		const char * what() const throw();//noexcept override;
	};
};

std::ostream &operator<<(std::ostream &os, Bureaucrat &bur);