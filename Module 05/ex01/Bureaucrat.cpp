#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(const std::string &name_, int grade_) : name(name_), grade(grade_) { checkNewGrade(grade); }

Bureaucrat::Bureaucrat(const Bureaucrat &rhs) : name(rhs.name), grade(rhs.grade) {}

Bureaucrat &Bureaucrat::operator=(const Bureaucrat &rhs)
{
	this->grade = rhs.grade;
	return *this;
}

Bureaucrat::~Bureaucrat() {}

const std::string &Bureaucrat::getName() const
{
	return name;
}

int Bureaucrat::getGrade() const
{
	return grade;
}

std::string Bureaucrat::getInfo() const
{
	std::ostringstream os;
	os << getName() << "\'s grade: " << getGrade();
	return os.str();
}

std::string Bureaucrat::getExInfo() const
{
	std::ostringstream os;
	os << "Bureaucrat \'" << getName() << "\' (grade " << getGrade() << ")";
	return os.str();
}

int Bureaucrat::checkNewGrade(int new_grade)
{
	if (new_grade < 1)
		throw Bureaucrat::ExceptionGradeTooHigh(getInfo());
	if (new_grade > 150)
		throw Bureaucrat::ExceptionGradeTooLow(getInfo());
	return new_grade;
}

void Bureaucrat::incrementGrade()
{
	checkNewGrade(--grade);
}

void Bureaucrat::decrementGrade()
{
	checkNewGrade(++grade);
}

std::ostream &operator<<(std::ostream &os, Bureaucrat &bur)
{
	return os << bur.getInfo();
}

Bureaucrat::ExceptionGradeTooHigh::ExceptionGradeTooHigh(const std::string &msg) throw() : message(msg) {}

Bureaucrat::ExceptionGradeTooHigh::~ExceptionGradeTooHigh() throw() {}

const char * Bureaucrat::ExceptionGradeTooHigh::what() const throw()
{
	return this->message.c_str();
}

Bureaucrat::ExceptionGradeTooLow::ExceptionGradeTooLow(const std::string &msg) throw() : message(msg) {}

Bureaucrat::ExceptionGradeTooLow::~ExceptionGradeTooLow() throw() {}

const char * Bureaucrat::ExceptionGradeTooLow::what() const throw()
{
	return this->message.c_str();
}