#include "Span.hpp"
#include <iostream>
#include <cstdlib>
#include <vector>

int main()
{
	{
		const int SIZE = 10000;
		std::vector<int> random_numbers;
		Span sp = Span(SIZE);
		
		random_numbers.reserve(SIZE);
		std::srand(std::time(NULL));
		for (int i = 0; i < SIZE; ++i)
			random_numbers.push_back(std::rand());
		sp.addNumbers(random_numbers.begin(), random_numbers.end());
		std::cout << sp.shortestSpan() << std::endl;
		std::cout << sp.longestSpan() << std::endl;
	}
	std::cout << std::endl;

	try
	{
		const int SIZE = 5;
		std::vector<int> random_numbers;
		Span sp = Span(SIZE);
		
		random_numbers.reserve(SIZE);
		std::srand(std::time(NULL));
		for (int i = 0; i < SIZE; ++i)
			random_numbers.push_back(std::rand() % 100);
		sp.addNumbers(random_numbers.begin(), random_numbers.end());
		std::cout << sp.shortestSpan() << std::endl;
		std::cout << sp.longestSpan() << std::endl;
		sp.addNumber(727);
	}
	catch (std::out_of_range& e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << std::endl;

	try
	{
		const int SIZE = 1;
		Span sp = Span(SIZE);
		
		sp.addNumber(727);
		std::cout << sp.shortestSpan() << std::endl;
		std::cout << sp.longestSpan() << std::endl;
		sp.addNumber(727);
	}
	catch (Span::OneElementException& e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << std::endl;

	try
	{
		const int SIZE = 2;
		Span sp = Span(SIZE);
		
		sp.addNumber(727);
		sp.addNumber(1727);
		std::cout << sp.shortestSpan() << std::endl;
		std::cout << sp.longestSpan() << std::endl;
		sp.addNumber(727);
	}
	catch (std::out_of_range& e)
	{
		std::cout << e.what() << std::endl;
	}
	std::cout << std::endl;
	return 0;
}