#include <list>
#include <ctime>
#include <stdexcept>
#include <iostream>

#ifndef UINT32_MAX
#define UINT32_MAX 4294967295U
#endif

class Span
{
public:
	Span(unsigned int _size);
	
	template<typename InputIt>
	Span(InputIt begin, InputIt end) : maxsize(end - begin), longest(0), shortest(UINT32_MAX), last_changed(0), last_longest(0), last_shortest(0)
	{
		for (InputIt it = begin; it != end; ++it)
			storage.push_back(*it);
		storage.sort();
	}
	Span(const Span&);
	~Span();
	void addNumber(int value);

	template<typename InputIt>
	void addNumbers(InputIt begin, InputIt end)
	{
		// std::cout << maxsize << storage.size() << (unsigned)(end - begin) << std::endl;
		if (maxsize - storage.size() < (unsigned)(end - begin))
			throw std::out_of_range("Span instance is full!");
		else
			for (InputIt it = begin; it != end; ++it)
				insertNumber(*it);
		updateTime(last_changed);
	}

	unsigned int longestSpan();
	unsigned int shortestSpan();

	class OneElementException : public std::logic_error
	{
	private:
		std::string message;
	public:
		OneElementException(const std::string &msg) throw();
		~OneElementException() throw();
		const char * what() const throw();
	};
private:
	const unsigned int maxsize;

	unsigned int longest;
	unsigned int shortest;

	time_t last_changed;
	time_t last_longest;
	time_t last_shortest;

	std::list<int> storage;
	
	void insertNumber(int value);
	void updateTime(time_t &);
};