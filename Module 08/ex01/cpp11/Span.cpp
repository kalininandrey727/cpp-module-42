#include "Span.hpp"

Span::Span(unsigned int _size) : maxsize(_size) {}

// template<typename InputIt>
// Span::Span(InputIt begin, InputIt end) : maxsize(end - begin)
// {
// 	storage.emplace_back(begin, end);
// 	storage.sort();
// 	// for (InputIt it = begin; it != end; ++it)
// 	// {
// 	// 	storage.push_back(*it);
// 	// }
// }

Span::Span(const Span& other) : maxsize(other.maxsize)
{
	storage.emplace_back(other.storage.begin(), other.storage.end());
}

Span::~Span() {}

void Span::insertNumber(int value)
{
	bool inserted = false;

	for (std::list<int>::const_iterator it = storage.cbegin(); it != storage.cend() && !inserted; ++it)
		if (value < *it)
		{
			storage.insert(it, value);
			inserted = true;
		}
	if (!inserted)
		storage.push_back(value);
}

void Span::addNumber(int value)
{
	if (storage.size() == maxsize)
		throw std::out_of_range("Span instance is full!");
	else
		insertNumber(value);
	updateTime(last_changed);
}

// template<typename InputIt>
// void Span::addNumbers(InputIt begin, InputIt end)
// {
// 	if (maxsize - storage.size() < end - begin)
// 		throw std::out_of_range("Span instance is full!");
// 	else
// 		for (InputIt it = begin; it != end; ++it)
// 			insertNumber(*it);
// 	updateTime(last_changed);
// }

unsigned int Span::longestSpan()
{
	if (last_longest < last_changed) // If [longest] is NOT up-to-date
	{
		longest = storage.back() - storage.front();
		updateTime(last_longest);
	}
	return longest;
}

unsigned int Span::shortestSpan()
{
	if (last_shortest < last_changed) // If [shortest] is NOT up-to-date
	{
		for (std::list<int>::iterator it = std::next(storage.begin()); it != storage.end(); ++it)
			if ((unsigned)(*it - *std::prev(it)) < shortest)
				shortest = *it - *std::prev(it);
		updateTime(last_shortest);
	}
	return shortest;
}

void Span::updateTime(std::time_t &last_)
{
	last_ = std::time(NULL);
}
