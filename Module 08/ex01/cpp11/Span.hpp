#include <list>
#include <chrono>
#include <stdexcept>

class Span
{
public:
	Span(unsigned int _size);
	
	template<typename InputIt>
	Span(InputIt begin, InputIt end) : maxsize(end - begin)
	{
		storage.emplace_back(begin, end);
		storage.sort();
	}
	Span(const Span&);
	~Span();
	void addNumber(int value);

	template<typename InputIt>
	void addNumbers(InputIt begin, InputIt end)
	{
		if (maxsize - storage.size() < end - begin)
			throw std::out_of_range("Span instance is full!");
		else
			for (InputIt it = begin; it != end; ++it)
				insertNumber(*it);
		updateTime(last_changed);
	}

	unsigned int longestSpan();
	unsigned int shortestSpan();
private:
	const unsigned int maxsize;

	unsigned int longest;
	unsigned int shortest;

	std::time_t last_changed;
	std::time_t last_longest;
	std::time_t last_shortest;

	std::list<int> storage;
	
	void insertNumber(int value);
	void updateTime(std::time_t &);
};