#include "Span.hpp"

Span::Span(unsigned int _size) : maxsize(_size), longest(0), shortest(UINT32_MAX), last_changed(0), last_longest(0), last_shortest(0) {}


Span::Span(const Span& other) : maxsize(other.maxsize), longest(0), shortest(UINT32_MAX), last_changed(0), last_longest(0), last_shortest(0) 
{
	for (std::list<int>::const_iterator it = other.storage.begin(); it != other.storage.end(); ++it)
		storage.push_back(*it);
	// storage.emplace_back(other.storage.begin(), other.storage.end());
}

Span::~Span() {}

void Span::insertNumber(int value)
{
	bool inserted = false;

	for (std::list<int>::iterator it = storage.begin(); it != storage.end() && !inserted; ++it)
		if (value < *it)
		{
			storage.insert(it, value);
			inserted = true;
		}
	if (!inserted)
		storage.push_back(value);
}

void Span::addNumber(int value)
{
	if (storage.size() == maxsize)
		throw std::out_of_range("Span instance is full!");
	else
		insertNumber(value);
	updateTime(last_changed);
}

unsigned int Span::longestSpan()
{
	if (storage.size() <= 1)
		throw Span::OneElementException("Can not find span.");
	if (last_longest < last_changed) // If [longest] is NOT up-to-date
	{
		longest = storage.back() - storage.front();
		updateTime(last_longest);
	}
	return longest;
}

unsigned int Span::shortestSpan()
{
	if (storage.size() <= 1)
		throw Span::OneElementException("Can not find span.");
	if (last_shortest < last_changed) // If [shortest] is NOT up-to-date
	{
		for (std::list<int>::iterator prev = storage.begin(), it = ++storage.begin(); it != storage.end(); ++it, ++prev)
			if ((unsigned)(*it - *prev) < shortest)
				shortest = *it - *prev;
		updateTime(last_shortest);
	}
	return shortest;
}

void Span::updateTime(time_t &last_)
{
	last_ = time(NULL);
}

Span::OneElementException::OneElementException(const std::string &msg) throw() : std::logic_error(msg), message(msg) {}
Span::OneElementException::~OneElementException() throw() {}

const char * Span::OneElementException::what() const throw()
{
	return this->message.c_str();
}