#include "MutantStack.hpp"
#include <iostream>

int main()
{
    MutantStack<int> st;
    for (int i = 0; i < 20; ++i)
        st.push(i);
    for (MutantStack<int>::iterator it = st.begin(); it != st.end(); ++it)
        std::cout << *it << std::endl;
}