#pragma once
#include <exception>
#include <stack>

template <typename T>
class MutantStack : public std::stack<T, std::deque<T> >
{
public:
	typedef typename std::deque<T>::iterator iterator;
	
	iterator begin() { return std::stack<T>::c.begin(); }
	iterator end() { return std::stack<T>::c.end(); }
	iterator begin() const { return std::stack<T>::c.begin(); }
	iterator end() const { return std::stack<T>::c.end(); }
};
