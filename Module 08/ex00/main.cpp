#include <iostream>
#include "easyfind.hpp"
#include <vector>
#include <algorithm>

int main( void )
{
	const int		 SIZE = 10;
	std::vector<int> example(SIZE);
	std::vector<int>::iterator iter;
	
	for (int i = 0; i < SIZE; ++i)
		example[i] = (2 * i + 1) % (SIZE) + i / (SIZE / 2) - 1;
	for (std::vector<int>::iterator it = example.begin() - 1; it != example.end() + 1; ++it)
		std::cout << *it << ' ';
	for (int i = -1; i < SIZE + 1; ++i)
	{
		iter = easyfind(example, i);
		if (iter != example.end())
			std::cout << "found value " << *iter << " at position " << iter - example.begin() << std::endl;
		else
			std::cout << "Could not find value " << i << " in container" << std::endl;
	}
}