template<typename T>
typename T::iterator easyfind(T& cont, int to_find)
{
	for (typename T::iterator it = cont.begin(); it != cont.end(); ++it)
		if (*it == to_find)
			return it;
	return cont.end();
}