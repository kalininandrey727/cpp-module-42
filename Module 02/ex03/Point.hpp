#pragma once
#include "Fixed.hpp"

class Point
{
private:
	Fixed x;
	Fixed y;

public:
	Point();
	Point(float x_, float y_);
	Point(const Point &);
	~Point();
	Point &operator=(Point &);
	const Fixed &get_x() const;
	const Fixed &get_y() const;
	void set_x(float x_);
	void set_y(float y_);
};

bool bsp( Point const a, Point const b, Point const c, Point const point);