#include <iostream>
#include <cstdlib>
#include "Point.hpp"

int main( void )
{
	std::srand(727);
	Point a(0, 0);
	Point b(10, 0);
	Point c(0, 10);
	Point point = Point();
	std::cout << "(" << point.get_x() << ", " << point.get_y() << "): "
			  << bsp(a, b, c, point) << std::endl;
	for (int i = 0; i < 10; ++i)
	{
		point.set_x((float)(std::rand() % 1000) / 100);
		point.set_y((float)(std::rand() % 1000) / 100);
		std::cout << "(" << point.get_x() << ", " << point.get_y() << "): "
				  << bsp(a, b, c, point) << std::endl;
	}
	return 0;
}