#include "Point.hpp"

Point::Point() : x(), y()
{

} 

Point::Point(float x_, float y_) : x(x_), y(y_)
{

}

Point::Point(const Point &copy)
{
	this->x = copy.x;
	this->y = copy.y;
}

Point::~Point()
{

}

Point &Point::operator=(Point &copy)
{
	this->x = copy.x;
	this->y = copy.y;
	return *this;
}

const Fixed &Point::get_x() const 
{
	return x;
}

const Fixed &Point::get_y() const
{
	return y;
}

void Point::set_x(float x_)
{
	x = x_;
}

void Point::set_y(float y_)
{
	y = y_;
}

float sign(Point const p1, Point const p2, Point const p3)
{
    return (p1.get_x().toFloat() - p3.get_x().toFloat()) * (p2.get_y().toFloat() - p3.get_y().toFloat())
		 - (p2.get_x().toFloat() - p3.get_x().toFloat()) * (p1.get_y().toFloat() - p3.get_y().toFloat());
}

bool bsp(Point const a, Point const b, Point const c, Point const point)
{
    float s = sign(a, point, c);
	float t = sign(b, point, a);

	if ((s < 0) != (t < 0) && s != 0 && t != 0)
        return false;

	float d = sign(c, point, b);
	
	return d == 0 || (d < 0) == (s + t <= 0);
}