#include "Fixed.hpp"

Fixed::Fixed() : value(0), FractionalPoint(8)
{
	std::cout << "Default contructor" << std::endl;
}

Fixed::Fixed(const int value_)  : value(value_ * 256), FractionalPoint(8)
{
	std::cout << "Integer contructor" << std::endl;

}
Fixed::Fixed(const float value_) : value(value_ * 256), FractionalPoint(8)
{
	std::cout << "Float contructor" << std::endl;

}

Fixed::~Fixed()
{
	std::cout << "Destructor" << std::endl;
}

Fixed::Fixed(const Fixed &copy) : value(copy.getRawBits()), FractionalPoint(8)
{
	std::cout << "Copy contructor" << std::endl;
}

Fixed &Fixed::operator=(const Fixed &copy)
{
	std::cout << "Copy assignment contructor" << std::endl;
	value = copy.getRawBits();
	return *this;
}

std::ostream &operator<<(std::ostream &os, const Fixed &fixed)
{
	return os << fixed.toFloat();
}

int Fixed::getRawBits( void ) const
{
	return value;
}

void Fixed::setRawBits( int const raw )
{
	value = raw;
}

float Fixed::toFloat( void ) const
{
	return ((float)value / 256);
}

int Fixed::toInt( void ) const
{
	return (value / 256);
}