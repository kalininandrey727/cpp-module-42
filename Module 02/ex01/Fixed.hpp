#pragma once

#include <iostream>
#include <string>

class Fixed
{
private:
	int value;
	const int FractionalPoint;
public:
	Fixed();
	Fixed(const int);
	Fixed(const float);
	Fixed(const Fixed &);
	~Fixed();

	Fixed &operator=(const Fixed &);

	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;
};

std::ostream &operator<<(std::ostream &, const Fixed &);