#pragma once

#include <iostream>
#include <string>

class Fixed
{
private:
	int value;
	const int FractionalPoint;
public:
	Fixed();
	Fixed(const int);
	Fixed(const float);
	Fixed(const Fixed &);
	~Fixed();

	Fixed &operator=(const Fixed &);

	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;

	bool operator<(const Fixed &) const;
	bool operator>(const Fixed &) const;
	bool operator==(const Fixed &) const;
	bool operator<=(const Fixed &) const;
	bool operator>=(const Fixed &) const;
	bool operator!=(const Fixed &) const;

	Fixed operator+(const Fixed &);
	Fixed operator-(const Fixed &);
	Fixed operator*(const Fixed &);
	Fixed operator/(const Fixed &);

	Fixed &operator++(void);
	Fixed operator++(int);
	Fixed &operator--(void);
	Fixed operator--(int);

	static Fixed &min(Fixed &lhs, Fixed &rhs);
	static const Fixed &min(const Fixed &lhs, const Fixed &rhs);
	static Fixed &max(Fixed &lhs, Fixed &rhs);
	static const Fixed &max(const Fixed &lhs, const Fixed &rhs);
};

std::ostream &operator<<(std::ostream &, const Fixed &);
