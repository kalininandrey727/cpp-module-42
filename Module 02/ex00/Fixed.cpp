#include "Fixed.hpp"

Fixed::Fixed() : value(0), FractionalPoint(8)
{
	std::cout << "Default contructor" << std::endl;
}

Fixed::~Fixed()
{
	std::cout << "Destructor" << std::endl;
}

Fixed::Fixed(Fixed &copy) : value(copy.value), FractionalPoint(8)
{
	std::cout << "Copy contructor" << std::endl;
}

Fixed &Fixed::operator=(const Fixed &copy)
{
	std::cout << "Copy assignment contructor" << std::endl;
	value = copy.getRawBits();
	return *this;
}

int Fixed::getRawBits( void ) const
{
	return value;
}

void Fixed::setRawBits( int const raw )
{
	value = raw;
}