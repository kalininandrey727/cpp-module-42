#pragma once

#include <iostream>
#include <string>

class Fixed
{
private:
	int value;
	const int FractionalPoint;
public:
	Fixed();
	~Fixed();
	Fixed(Fixed &);

	Fixed &operator=(const Fixed &);

	int getRawBits( void ) const;
	void setRawBits( int const raw );
};
