#include <iostream>
#include <cctype>
#include <cstring>

int main(int ac, char **av)
{
	int counter;

	if (ac == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	else
	{
		for (int i = 1; i < ac; ++i)
		{
			counter = -1;
			while (av[i][++counter])
				std::cout << (char)std::toupper(av[i][counter]);
		}
	}
	std::cout << "\n";
}