#include <cwchar>
#include <iostream>
#include <iomanip>
#include <ctime>
#include "Account.hpp"

int	Account::_nbAccounts			= 0;
int	Account::_totalAmount			= 0;
int	Account::_totalNbDeposits		= 0;
int	Account::_totalNbWithdrawals	= 0;

int	Account::getNbAccounts( void )
{
	return Account::_nbAccounts;
}

int	Account::getTotalAmount( void )
{
	return Account::_totalAmount;
}

int	Account::getNbDeposits( void )
{
	return Account::_totalNbDeposits;
}

int	Account::getNbWithdrawals( void )
{
	return Account::_totalNbWithdrawals;
}

void	Account::displayAccountsInfos( void )
{
	Account::_displayTimestamp();
	std::cout 	<< "accounts:"		<< Account::getNbAccounts()
				<< ";total:"		<< Account::getTotalAmount()
				<< ";deposits:"		<< Account::getNbDeposits()
				<< ";withdrawals:"	<< Account::getNbWithdrawals()
				<< std::endl;
}

Account::Account( int initial_deposit )
{
	Account::_accountIndex = Account::_nbAccounts++;
	Account::_amount = initial_deposit;
	Account::_totalAmount += initial_deposit;
	Account::_nbDeposits = 0;
	Account::_nbWithdrawals = 0;

	Account::_displayTimestamp();
	std::cout	<< "index:"		<< Account::_accountIndex
				<< ";amount:"	<< Account::_amount
				<< ";created"	<< std::endl;
}

Account::Account( void )
{
	Account::_accountIndex = Account::_nbAccounts++;
	Account::_amount = 0;
	Account::_nbDeposits = 0;
	Account::_nbWithdrawals = 0;

	Account::_displayTimestamp();
	std::cout	<< "index:"		<< Account::_accountIndex
				<< ";amount:"	<< Account::_amount
				<< ";created"	<< std::endl;
}

Account::~Account( void )
{
	Account::_displayTimestamp();
	--Account::_nbAccounts;
	std::cout	<< "index:"		<< Account::_accountIndex
				<< ";amount:"	<< Account::_amount
				<< ";closed"	<< std::endl;
}

void	Account::makeDeposit( int deposit )
{
	Account::_displayTimestamp();
	std::cout 	<< "index:"			<< Account::_accountIndex
				<< ";p_amount:"		<< Account::_amount
				<< ";deposit:"		<< deposit
				<< ";amount:"		<< Account::_amount + deposit
				<< ";nb_deposits:"	<< ++Account::_nbDeposits
				<< std::endl;
	
	++Account::_totalNbDeposits;
	Account::_amount += deposit;
	Account::_totalAmount += deposit;
}

bool	Account::makeWithdrawal( int withdrawal )
{
	Account::_displayTimestamp();
	std::cout 	<< "index:"			<< Account::_accountIndex
				<< ";p_amount:"		<< Account::_amount
				<< ";withdrawal:";
	if (withdrawal > Account::_amount)
		std::cout << "refused";
	else
	{
		std::cout << withdrawal;
		_amount -= withdrawal;
		Account::_totalAmount -= withdrawal;
		std::cout	<< ";amount:" << Account::_amount
					<< ";nb_withdrawals:" << ++Account::_nbWithdrawals;
		++Account::_totalNbWithdrawals;
	}
	std::cout << std::endl;
	return (withdrawal <= Account::_amount);
}

int		Account::checkAmount( void ) const
{
	return Account::_amount > 0;
}

void	Account::displayStatus( void ) const
{
	Account::_displayTimestamp();
	std::cout 	<< "index:"			<< Account::_accountIndex
				<< ";amount:"		<< Account::_amount
				<< ";deposits:"		<< Account::_nbDeposits
				<< ";withdrawals:"	<< Account::_nbWithdrawals
				<< std::endl;
}

void	Account::_displayTimestamp( void )
{
	//std::cout << "[19920104_091532] ";
	std::time_t result = std::time(NULL);
	std::tm* ts = std::localtime(&result);
	std::cout	<< "["
				<< 1900 + ts->tm_year
				<< std::setfill('0') << std::setw(2) << 1 + ts->tm_mon
				<< std::setfill('0') << std::setw(2) << ts->tm_mday
				<< "_"
				<< std::setfill('0') << std::setw(2) << ts->tm_hour
				<< std::setfill('0') << std::setw(2) << ts->tm_min
				<< std::setfill('0') << std::setw(2) << ts->tm_sec
				<< "] ";
}