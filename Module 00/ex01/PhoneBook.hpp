#pragma once
#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include "Contact.hpp"
#include <cstddef>

class PhoneBook
{
public:
	Contact contacts[8];
	int	size;

	void AddContact(Contact contact);
	void SearchByIndex(int i);
	void MoveBy1();
	void OutputAll();
};

#endif