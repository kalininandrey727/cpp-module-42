#pragma once
#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <iostream>
#include <iomanip>
#include <string>

class Contact
{
public:
	std::string first_name;
	std::string last_name;
	std::string nickname;
	std::string phone_number;
	std::string darkest_secret;
	Contact(std::string fn, std::string ln, std::string nn, std::string pn, std::string ds);
	Contact();
	void ShortOutput();
	void FullOutput();
};

std::string ShrinkToWidth(std::string str);

#endif