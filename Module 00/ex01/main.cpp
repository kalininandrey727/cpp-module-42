#include "PhoneBook.hpp"
#include <iostream>

int main()
{
	PhoneBook pb = PhoneBook();
	std::string command;
	std::string fn;
	std::string ln;
	std::string nn;
	std::string pn;
	std::string ds;
	int index;
	while (true)
	{
		std::cin >> command;
		if (command == "ADD")
		{
			std::cout << "Input first name: ";
			std::cin >> fn;
			std::cout << "Input last name: ";
			std::cin >> ln;
			std::cout << "Input nickname: ";
			std::cin >> nn;
			std::cout << "Input phone number: ";
			std::cin >> pn;
			std::cout << "Input darkest secret: ";
			std::cin >> ds;
			pb.AddContact(Contact(fn, ln, nn, pn, ds));
		}
		else if (command == "SEARCH")
		{
			pb.OutputAll();
			std::cout << "Input desired contact's index: ";
			std::cin >> index;
			pb.SearchByIndex(index);
		}
		else if (command == "EXIT")
			return (0);
	}
}