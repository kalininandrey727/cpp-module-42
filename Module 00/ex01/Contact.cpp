#include "Contact.hpp"

Contact::Contact(std::string fn, std::string ln, std::string nn, std::string pn, std::string ds) : 
	first_name(fn), last_name(ln), nickname(nn), phone_number(pn), darkest_secret(ds) 
{}

Contact::Contact()
{}

void Contact::ShortOutput()
{
	if (first_name == "")
		std::cout << "Something wrong, I can feel it.\n";
	else
		std::cout << std::setw(10) << ShrinkToWidth(first_name) << "|" 
				  << std::setw(10) << ShrinkToWidth(last_name) << "|" 
				  << std::setw(10) << ShrinkToWidth(nickname) << "|" 
				  << std::setw(10) << ShrinkToWidth(phone_number) << std::endl;
}

void Contact::FullOutput()
{
	if (first_name == "")
		std::cout << "Something wrong, I can feel it.\n";
	else
		std::cout << first_name << std::endl 
				  << last_name << std::endl
				  << nickname << std::endl
				  << phone_number << std::endl
				  << darkest_secret << std::endl;
}

std::string ShrinkToWidth(std::string str)
{
	if (str.size() < 10)
		return str;
	std::string shrinked;
	shrinked = str.substr(0, 9);
	shrinked.push_back('.');
	return shrinked;
}
