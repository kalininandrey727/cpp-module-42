#include "PhoneBook.hpp"

void PhoneBook::MoveBy1()
{
	for (int i = 0; i < size - 1; ++i)
		contacts[i] = contacts[i + 1];
	--size;
}

void PhoneBook::OutputAll()
{
	for (int i = 0; i < size; ++i)
		contacts[i].ShortOutput();
}

void PhoneBook::AddContact(Contact contact)
{
	if (size == 8)
		MoveBy1();
	contacts[size++] = contact;
}

void PhoneBook::SearchByIndex(int i)
{
	if (i < 0 || i > size)
		std::cout << "Something wrong, I can feel it.\n";
	else
		PhoneBook::contacts[i].FullOutput();
}