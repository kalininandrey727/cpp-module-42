#include "Zombie.hpp"

Zombie::Zombie() : name("") {}
Zombie::Zombie(const std::string _name) : name(_name) {}

Zombie::~Zombie()
{
	std::cout << "And now there's " << name << "! He's dead.\n";
}

void Zombie::announce()
{
	std::cout << name << ": BraiiiiiiinnnzzzZ...\n";
}

void Zombie::setName(const std::string name)
{
	Zombie::name = name;
}