#include <iostream>
#include <string>

int	main( void )
{
	std::string brain = "HI THIS IS BRAIN";
	std::string* stringPTR = &brain;
	std::string& stringREF = brain;

	std::cout 	<< "string address:    " << &brain 			<< std::endl
				<< "stringPTR address: " << &(*stringPTR)	<< std::endl
				<< "stringREF address: " << &stringREF		<< std::endl
				<< "string value:      " << brain			<< std::endl
				<< "stringPTR value:   " << *stringPTR		<< std::endl
				<< "stringREF value:   " << stringREF		<< std::endl;
}
