#include "HumanB.hpp"
#include "Weapon.hpp"
#include <string>

HumanB::HumanB(const std::string &name_) : name(name_), weapon(NULL)
{
	
}

void HumanB::attack()
{
	std::cout << name << " attacks with their";
	if (weapon)
		std::cout << " " << weapon->getType();
	else
		std::cout << "... Wait! " << name << " doesn\'t have a weapon!";
	std::cout << std::endl;
}

void HumanB::setWeapon(Weapon &weapon_)
{
	weapon = &weapon_;
}
