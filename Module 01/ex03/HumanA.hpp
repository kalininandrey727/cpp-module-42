#pragma once
#include "Weapon.hpp"
#include <iostream>
#include <string>

class HumanA
{
private:
	const std::string name;
	Weapon &weapon;
public:
	HumanA(const std::string &, Weapon &);
	void attack();
};