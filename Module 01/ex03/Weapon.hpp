#pragma once
#include <iostream>
#include <string>

class Weapon
{
private:
	std::string type;
public:
	Weapon(const std::string& new_type);
	const std::string& getType();
	void setType(std::string new_type);
};