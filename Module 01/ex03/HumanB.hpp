#pragma once
#include "Weapon.hpp"
#include <iostream>
#include <string>

class HumanB
{
private:
	const std::string name;
	Weapon *weapon;
public:
	HumanB(const std::string &);
	void attack();
	void setWeapon(Weapon &weapon_);
};