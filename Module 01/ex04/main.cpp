#include "Replace.hpp"

#include <iostream>
#include <string>

int main(int ac, char **av)
{
	if (ac != 4)
	{
		std::cerr << "Wrong argument count: expected 3, got " << ac - 1 << std::endl;
		return 1;
	}
	Replacer rep(av[1], av[2], av[3]);
	rep.execute();
}

