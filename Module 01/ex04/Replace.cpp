#include "Replace.hpp"
#include <fstream>
#include <ios>
Replacer::Replacer(std::string fn_, std::string s1_, std::string s2_) : fn(fn_), s1(s1_), s2(s2_) {}

bool Replacer::execute()
{
	size_t pos = 0;
	std::string buf;
	if (!fn.size())
	{
		std::cerr << "Wrong filename" << std::endl;
		return false;
	}
	if (!s1.size() || !s2.size())
	{
		std::cerr << "Strings must not be empty" << std::endl;
		return false;
	}
	fsin = std::ifstream(fn);
	if (!fsin)
	{
		std::cerr << "Could not open " << fn << std::endl;
		//fsout.close();
		return false;
	}
	fsout = std::ofstream(fn + ".replace", std::ios::ios_base::in | std::ios::trunc);
	if (!fsout)
	{
		std::cerr << "Could not open " << fn << ".replace" << std::endl;
		fsin.close();
		return false;
	}
	std::getline(fsin, buf, '\0');
	pos = buf.find(s1);
	while (pos != std::string::npos)
	{
		buf.erase(pos, s1.size());
		buf.insert(pos, s2);
		pos = buf.find(s1, pos + s2.size());
	}
	fsout << buf;
	fsout.close();
	fsin.close();
	return true;
}