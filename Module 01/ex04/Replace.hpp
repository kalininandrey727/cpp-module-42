#pragma once

#include <iostream>
#include <fstream>
#include <string>

class Replacer
{
private:
	std::string fn;
	std::string s1;
	std::string s2;
	std::ifstream fsin;
	std::ofstream fsout;
public:
	Replacer(std::string fn_, std::string s1_, std::string s2_);
	bool execute();
};