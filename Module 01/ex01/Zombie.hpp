#pragma once
#include <iostream>
#include <string>

class Zombie
{
private:
	std::string name;

public:
	Zombie();
	Zombie(const std::string _name);
	~Zombie();
	void announce();
	void setName(const std::string name);
};

Zombie* zombieHorde( int N, std::string name );
