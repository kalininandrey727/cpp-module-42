#include "Harl.hpp"

#include <iostream>
#include <string>

int main()
{
	Harl Harl;
	std::string message = "";
	while (message != "stop")
	{
		std::cout << "Input message type: ";
		std::cin >> message;
		Harl.complain(message);
	}
}

