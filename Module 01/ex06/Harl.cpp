#include "Harl.hpp"

Harl::Harl(std::string level) : HashedLevel(thisIsNotReallyAHash(level))
{

}

void Harl::debug( void )
{
	std::cout << "[DEBUG]\nI love having extra bacon for my 7XL-double-cheese-triple-pickle-special-ketchup burger.\nI really do!\n" << std::endl;
}

void Harl::info( void )
{
	std::cout << "[INFO]\nI cannot believe adding extra bacon costs more money.\nYou didn’t put enough bacon in my burger!\nIf you did, I wouldn’t be asking for more!\n" << std::endl;
}

void Harl::warning( void )
{
	std::cout << "[WARNING]\nI think I deserve to have some extra bacon for free.\nI’ve been coming for years whereas you started working here since last month.\n" << std::endl;
}

void Harl::error( void )
{
	std::cout << "[ERROR]\nThis is unacceptable! I want to speak to the manager now.\n" << std::endl;
}

void Harl::insignificant( void )
{
	std::cout << "[ Probably complaining about insignificant problems ]" << std::endl;
}

int Harl::thisIsNotReallyAHash(std::string level)
{
	return (0b100		* (level == "ERROR")
		  | 0b11	* (level == "WARNING")
		  | 0b10	* (level == "INFO")
		  | 0b1		* (level == "DEBUG"));
}

void Harl::complain()
{
	switch (HashedLevel)
	{
		case Harl::Debug:
			debug();
		case Harl::Info:
			info();
		case Harl::Warning:
			warning();
		case Harl::Error:
			error();
			break;
		default:
			insignificant();
	}
}