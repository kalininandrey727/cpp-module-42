#include "Harl.hpp"

int main(int ac, char **av)
{
	std::string level = "";
	if (ac == 2)
		level = av[1];
	Harl Harl(level);
	Harl.complain();
}

