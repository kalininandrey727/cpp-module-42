#pragma once
#include <iostream>
#include <string>

class Harl
{
private:
	int HashedLevel;
	void insignificant( void );
	void debug( void );
	void info( void );
	void warning( void );
	void error( void );
	int thisIsNotReallyAHash(std::string level);
public:
	Harl(std::string level);
	void complain();
	enum Levels {
		Debug = 1,
		Info = 2,
		Warning = 4,
		Error = 8
	};
};