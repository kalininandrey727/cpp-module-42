#include "Cat.hpp"

Cat::Cat() : Animal()
{
	this->type = "Cat";
	std::cout << type << " is created!" << std::endl;
}

Cat::Cat(const Cat &rhs) : Animal()
{
	*this = rhs;
}

Cat &Cat::operator=(const Cat &rhs)
{
	this->type = rhs.type;
	return *this;
}

Cat::~Cat() 
{
	std::cout << "Cat is destroyed!" << std::endl;
}

void Cat::makeSound() const
{
	std::cout << "Meoow... I guess..." << std::endl;
}