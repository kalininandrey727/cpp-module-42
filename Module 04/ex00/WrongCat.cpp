#include "WrongCat.hpp"

WrongCat::WrongCat() : WrongAnimal()
{
	this->type = "WrongCat";
	std::cout << type << " is created!" << std::endl;
}

WrongCat::WrongCat(const WrongCat &rhs) : WrongAnimal()
{
	*this = rhs;
}

WrongCat &WrongCat::operator=(const WrongCat &rhs)
{
	this->type = rhs.type;
	return *this;
}

WrongCat::~WrongCat() 
{
	std::cout << "WrongCat is destroyed!" << std::endl;
}

void WrongCat::makeSound() const
{
	std::cout << "Meoow... I guess..." << std::endl;
}