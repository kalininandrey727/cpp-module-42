#include "Animal.hpp"

Animal::Animal() : type("Animal")
{
	std::cout << type << " is created!" << std::endl;
}

Animal::Animal(const Animal &rhs)
{
	this->type = rhs.type;
}

Animal &Animal::operator=(const Animal &rhs)
{
	this->type = rhs.type;
	return *this;
}

Animal::~Animal() 
{
	std::cout << "Animal is destroyed!" << std::endl;
}

void Animal::makeSound() const
{
	std::cout << "[ THE ETERNAL SCREAMS OF PAIN AND AGONY ]" << std::endl;
}

const std::string &Animal::getType() const
{
	return type;
}
