#include "Dog.hpp"

Dog::Dog() : Animal()
{
	this->type = "Dog";
	std::cout << type << " is created!" << std::endl;
}

Dog::Dog(const Dog &rhs) : Animal()
{
	this->type = rhs.type;
}

Dog &Dog::operator=(const Dog &rhs)
{
	this->type = rhs.type;
	return *this;
}

Dog::~Dog() 
{
	std::cout << "Dog is destroyed!" << std::endl;
}

void Dog::makeSound() const
{
	std::cout << "Bark!!! Bark!!! I love you Human!!!" << std::endl;
}