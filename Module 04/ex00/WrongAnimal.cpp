#include "WrongAnimal.hpp"

WrongAnimal::WrongAnimal() : type("WrongAnimal")
{
	std::cout << type << " is created!" << std::endl;
}

WrongAnimal::WrongAnimal(const WrongAnimal &rhs)
{
	this->type = rhs.type;
}

WrongAnimal &WrongAnimal::operator=(const WrongAnimal &rhs)
{
	this->type = rhs.type;
	return *this;
}

WrongAnimal::~WrongAnimal() 
{
	std::cout << "WrongAnimal is destroyed!" << std::endl;
}

void WrongAnimal::makeSound() const
{
	std::cout << "[ THE ETERNAL SCREAMS OF PAIN AND AGONY ]" << std::endl;
}

const std::string &WrongAnimal::getType() const
{
	return type;
}
