#pragma once
#include "AMateria.hpp"

class Fire : public AMateria     // Done?
{
public:
	Fire();
	Fire(const Fire &rhs);
	~Fire();
	Fire& operator=(const Fire &rhs);
	virtual AMateria* clone() const;
	virtual void use(ICharacter& target);
};