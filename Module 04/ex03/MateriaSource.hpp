#pragma once
#include "IMateriaSource.hpp"

class MateriaSource : public IMateriaSource
{
protected:
	const static int	INVENTORY_SIZE = 4;
	int					equipped_slots;
	AMateria*			inventory[INVENTORY_SIZE];

public:
	MateriaSource();
	MateriaSource(const MateriaSource &rhs);
	~MateriaSource();
	MateriaSource& operator=(const MateriaSource &rhs);

	virtual void learnMateria(AMateria* materia);
	virtual AMateria* createMateria(std::string const &type);
};