#include "Cure.hpp"

Cure::Cure() : AMateria("cure") {}

Cure::Cure(const Cure &rhs) : AMateria(rhs.getType()) {}

Cure::~Cure() {}

Cure& Cure::operator=(const Cure &rhs)
{
	this->materia_type = rhs.getType();
	return *this;
}

AMateria* Cure::clone() const
{
	return new Cure();
}

void Cure::use(ICharacter& target)
{
	std::cout << " heals " << target.getName() << "\'s wounds" << std::endl;
}
