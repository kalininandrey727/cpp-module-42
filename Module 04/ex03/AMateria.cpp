#include "AMateria.hpp"

AMateria::AMateria(std::string const & type) : materia_type(type) {}

AMateria::~AMateria() {}

std::string const & AMateria::getType() const { return materia_type; }