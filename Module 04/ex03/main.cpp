#include "MateriaSource.hpp"
#include "Character.hpp"
#include "Ice.hpp"
#include "Fire.hpp"
#include "Cure.hpp"

int main( void )
{
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());
	src->learnMateria(new Fire());
	ICharacter* me = new Character("me");
	AMateria* tmp;
	tmp = src->createMateria("ice");
	me->equip(tmp);						// 0
	tmp = src->createMateria("cure");
	me->equip(tmp);						// 1
	tmp = src->createMateria("fire");
	me->equip(tmp);						// 2
	me->equip(tmp->clone());			// 3
	me->equip(tmp);						// 4
	ICharacter* bob = new Character("bob");
	me->use(-1, *bob);
	me->use(0, *bob);
	me->use(1, *bob);
	me->use(2, *bob);
	me->use(3, *bob);
	me->use(4, *bob);
	delete bob;
	delete me;
	delete src;
	return 0;
}