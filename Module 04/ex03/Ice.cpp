#include "Ice.hpp"

Ice::Ice() : AMateria("ice") {}

Ice::Ice(const Ice &rhs) : AMateria(rhs.getType()) {}

Ice::~Ice() {}

Ice& Ice::operator=(const Ice &rhs)
{
	this->materia_type = rhs.getType();
	return *this;
}

AMateria* Ice::clone() const
{
	return new Ice();
}

void Ice::use(ICharacter& target)
{
	std::cout << " shoots an ice bolt at " << target.getName() << std::endl;
}
