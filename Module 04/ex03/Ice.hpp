#pragma once
#include "AMateria.hpp"

class Ice : public AMateria     // Done?
{
public:
	Ice();
	Ice(const Ice &rhs);
	~Ice();
	Ice& operator=(const Ice &rhs);
	virtual AMateria* clone() const;
	virtual void use(ICharacter& target);
};