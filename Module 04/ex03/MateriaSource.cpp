#include "MateriaSource.hpp"

MateriaSource::MateriaSource() : equipped_slots(0) {}

MateriaSource::MateriaSource(const MateriaSource &rhs) : equipped_slots(rhs.equipped_slots)
{
    for (int i = 0; i < equipped_slots; ++i)
        inventory[i] = rhs.inventory[i];
}

MateriaSource::~MateriaSource()
{
    for (int i = 0; i < equipped_slots; ++i)
        delete inventory[i];
}

MateriaSource& MateriaSource::operator=(const MateriaSource &rhs)
{
    equipped_slots = rhs.equipped_slots;
    for (int i = 0; i < equipped_slots; ++i)
        inventory[i] = rhs.inventory[i];
    return *this;
}

void MateriaSource::learnMateria(AMateria* materia)
{
    bool is_not_there = true;
    if (equipped_slots < INVENTORY_SIZE)
    {
        for (int i = 0; (i < equipped_slots) && is_not_there; ++i)
            if (inventory[i]->getType() == materia->getType())
                is_not_there = false;
        if (is_not_there)
            inventory[equipped_slots++] = materia;
    }
}

AMateria* MateriaSource::createMateria(std::string const &type)
{
    for (int i = 0; i < equipped_slots; ++i)
        if (inventory[i]->getType() == type)
            return inventory[i]->clone();
    return 0;
}
