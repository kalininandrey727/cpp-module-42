#include "Fire.hpp"

Fire::Fire() : AMateria("fire") {}

Fire::Fire(const Fire &rhs) : AMateria(rhs.getType()) {}

Fire::~Fire() {}

Fire& Fire::operator=(const Fire &rhs)
{
	this->materia_type = rhs.getType();
	return *this;
}

AMateria* Fire::clone() const
{
	return new Fire();
}

void Fire::use(ICharacter& target)
{
	std::cout << " shoots a fireball at " << target.getName() << std::endl;
}
