#pragma once
#include "AMateria.hpp"
#include "ICharacter.hpp"
#include <iostream>

class Character : public ICharacter // Done
{
protected:
	const static int INVENTORY_SIZE = 4;
	std::string character_name;
	int equipped_slots;
	AMateria*	inventory[INVENTORY_SIZE];
public:
	Character(const std::string &name);
	Character(const Character &rhs);
	~Character();
	Character& operator=(const Character &rhs);
	
	virtual std::string const & getName() const;
	virtual void 				equip(AMateria* m);
	virtual void				unequip(int idx);
	virtual void				use(int idx, ICharacter& target);
};