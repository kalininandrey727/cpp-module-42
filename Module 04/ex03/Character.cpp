#include "Character.hpp"

Character::Character(const std::string &name) : character_name(name), equipped_slots(0) {}

Character::Character(const Character &rhs) : character_name(rhs.character_name), equipped_slots(rhs.equipped_slots)
{
    for (int i = 0; i < equipped_slots; ++i)
        inventory[i] = rhs.inventory[i];
}

Character::~Character()
{
    for (int i = 0; i < INVENTORY_SIZE; ++i)
        delete inventory[i];
}

Character& Character::operator=(const Character &rhs)
{
    character_name = rhs.character_name;
    equipped_slots = rhs.equipped_slots;
    for (int i = 0; i < equipped_slots; ++i)
        inventory[i] = rhs.inventory[i];
    return *this;
}

std::string const & Character::getName() const { return character_name; }

void Character::equip(AMateria* m)
{
    if (equipped_slots < INVENTORY_SIZE)
        inventory[equipped_slots++] = m;
}

void Character::unequip(int idx)
{
    if (0 <= idx && idx < equipped_slots)
    {
        --equipped_slots;
        for (int i = idx; i < equipped_slots; ++i)
            inventory[i] = inventory[i + 1];
    }
}

void Character::use(int idx, ICharacter& target)
{
    if (0 <= idx && idx < equipped_slots)
    {       
        std::cout << this->getName();
        inventory[idx]->use(target);
    }
}
