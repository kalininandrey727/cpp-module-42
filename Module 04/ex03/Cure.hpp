#pragma once
#include "AMateria.hpp"

class Cure : public AMateria		// Done?
{
public:
	Cure();
	Cure(const Cure &rhs);
	~Cure();
	Cure& operator=(const Cure &rhs);
	virtual AMateria* clone() const;
	virtual void use(ICharacter& target);
};