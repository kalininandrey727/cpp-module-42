#pragma once
#include "Animal.hpp"

class Dog : public Animal
{
private:
	Brain* brain;
public:
	Dog();
	Dog(const Dog &);
	Dog &operator=(const Dog &);
	virtual ~Dog();
	virtual void makeSound() const;
	virtual Brain *getBrain();
};