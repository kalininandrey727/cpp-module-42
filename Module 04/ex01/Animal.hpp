#pragma once
#include "Brain.hpp"

class Animal
{
protected:
	std::string type;
public:
	Animal();
	Animal(const Animal &);
	Animal &operator=(const Animal &);
	virtual ~Animal();
	const std::string &getType() const;
	virtual void makeSound() const;
	virtual Brain *getBrain() = 0;
};