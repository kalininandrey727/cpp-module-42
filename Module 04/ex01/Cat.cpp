#include "Cat.hpp"

Cat::Cat() : Animal()
{
	this->type = "Cat";
	brain = new Brain();
	std::cout << type << " is created!" << std::endl;
}

Cat::Cat(const Cat &rhs) : Animal()
{
	this->type = rhs.type;
	this->brain = new Brain(*rhs.brain);
	std::cout << "Copy Cat is created!" << std::endl;
}

Cat &Cat::operator=(const Cat &rhs)
{
	this->type = rhs.type;
	this->brain = new Brain(*rhs.brain);
	std::cout << "Copy Assignment Cat is created!" << std::endl;
	return *this;
}

Cat::~Cat() 
{
	delete brain;
	std::cout << "Cat is destroyed!" << std::endl;
}

void Cat::makeSound() const
{
	std::cout << "Meoow... I guess..." << std::endl;
}

Brain *Cat::getBrain()
{
	return brain;
}