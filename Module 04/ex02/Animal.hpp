#pragma once
#include "Brain.hpp"

class Animal
{
protected:
	std::string type;
public:
	virtual Animal &operator=(const Animal &) = 0;
	virtual ~Animal();
	const std::string &getType() const;
	virtual void makeSound() const = 0;
	virtual Brain *getBrain() const = 0;
};