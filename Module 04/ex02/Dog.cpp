#include "Dog.hpp"

Dog::Dog()
{
	this->type = "Dog";
	brain = new Brain();
	std::cout << type << " is created!" << std::endl;
}

Dog::Dog(const Dog &rhs)
{
	this->type = rhs.type;
	this->brain = new Brain(*rhs.brain);
	std::cout << "Copy Dog is created!" << std::endl;
}

Dog &Dog::operator=(const Dog &rhs)
{
	this->type = rhs.type;
	this->brain = new Brain(*rhs.brain);
	std::cout << "Copy Assignment Dog is created!" << std::endl;
	return *this;
}

Dog::~Dog() 
{
	delete brain;
	std::cout << "Dog is destroyed!" << std::endl;
}

void Dog::makeSound() const
{
	std::cout << "Bark!!! Bark!!! I love you Human!!!" << std::endl;
}

Animal &Dog::operator=(Animal const &rhs)
{
	std::cout << "Animal Assignement operator for Dog called" << std::endl;
	this->type = rhs.getType();
	*(this->brain) = *(rhs.getBrain());
	return *this;
}

Brain *Dog::getBrain() const
{
	return brain;
}