#pragma once
#include <iostream>
#include <sstream>
#include <string>

class Brain
{
public:
	Brain();
	Brain(const Brain &);
	Brain &operator=(const Brain &);
	virtual ~Brain();
	std::string ideas[100];
};