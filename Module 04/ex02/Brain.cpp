#include "Brain.hpp"

Brain::Brain()
{
	for (int i = 0; i < 100; ++i)
	{
		std::ostringstream os;
		os << i + 1;
		ideas[i] = os.str();
	}
	std::cout << "Brain is created!" << std::endl;
}

Brain::Brain(const Brain &rhs)
{
	for (int i = 0; i < 100; ++i)
		ideas[i] = rhs.ideas[i];
	std::cout << "Copy Brain is created!" << std::endl;
}

Brain &Brain::operator=(const Brain &rhs)
{
	for (int i = 0; i < 100; ++i)
		ideas[i] = rhs.ideas[i];
	std::cout << "Copy Assignment Brain is created!" << std::endl;
	return *this;
}

Brain::~Brain() 
{
	std::cout << "Brain is destroyed!" << std::endl;
}
