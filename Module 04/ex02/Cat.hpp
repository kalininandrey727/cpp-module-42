#pragma once
#include "Animal.hpp"

class Cat : public Animal
{
private:
	Brain* brain;
public:
	Cat();
	Cat(const Cat &);
	Cat &operator=(const Cat &);
	virtual ~Cat();
	virtual void makeSound() const;
	virtual Brain *getBrain() const;
	virtual Animal &operator=(Animal const &rhs);
};