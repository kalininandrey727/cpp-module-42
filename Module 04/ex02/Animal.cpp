#include "Animal.hpp"

Animal::~Animal() 
{
	std::cout << "Animal is destroyed!" << std::endl;
}

const std::string &Animal::getType() const
{
	return type;
}
