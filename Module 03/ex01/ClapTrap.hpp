#pragma once
#include <iostream>
#include <string>

class ClapTrap
{
protected:
	std::string name;
	int health;
	int energy;
	int max_health;
	int damage;
	ClapTrap();
public:
	ClapTrap(const std::string name_);
	~ClapTrap();
	void attack(const std::string& target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);
	bool IsWorking();
	int getHealth();
	int getEnergy();
};