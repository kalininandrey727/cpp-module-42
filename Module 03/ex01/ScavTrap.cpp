#include "ScavTrap.hpp"

ScavTrap::ScavTrap(std::string name_) : ClapTrap(name_)
{
	this->max_health = 100;
	this->energy = 50;
	this->damage = 20;
	this->health = max_health;
	std::cout << "ScavTrap " << name << " created!" << std::endl;
}

ScavTrap::~ScavTrap()
{
	std::cout << "ScavTrap " << name << " destroyed!" << std::endl;
}

void ScavTrap::guardGate()
{
	if (IsWorking())
		std::cout << "ScavTrap " << name << " is now in guard mode! (Why this even exists, it literally has zero functionality)" << std::endl;
	else
		std::cout << "lmao, ScavTrap " << name << " can\'t even enter guard mode!" << std::endl;

}