#include "ScavTrap.hpp"
#include <cstdlib>

int main( void )
{
	ScavTrap st("Bubi");
	std::srand(72727);
	st.guardGate();
	while (st.IsWorking())
	{
		st.attack("demon");
		st.takeDamage(std::rand() % 25);
		st.beRepaired(std::rand() % 25);
		std::cout << "Current energy points: " << st.getEnergy() << ", health points: " << st.getHealth() << std::endl;
	}
	return 0;
}