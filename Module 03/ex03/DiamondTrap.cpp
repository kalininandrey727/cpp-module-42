#include "DiamondTrap.hpp"

DiamondTrap::DiamondTrap( const std::string &name )
{
	ClapTrap::name = name + "_clap_name";
	this->_name = name;
	this->max_health = FragTrap::max_health;
	this->energy = ScavTrap::energy;
	this->damage = FragTrap::damage;
	std::cout << "Name constructor for DiamondTrap called" << std::endl;
	return;
}

DiamondTrap::DiamondTrap(void)
{
	std::cout << "Default constructor for DiamondTrap called" << std::endl;
	return;
}

DiamondTrap::DiamondTrap(DiamondTrap const &src) : ClapTrap(), FragTrap(), ScavTrap()
{
	std::cout << "Copy constructor for DiamondTrap called" << std::endl;
	*this = src;
	return;
}

DiamondTrap::~DiamondTrap( void )
{
	std::cout << "Destructor for DiamondTrap called" << std::endl;
	return;
}

DiamondTrap&	DiamondTrap::operator=(DiamondTrap const & rhs)
{
	ClapTrap::name = rhs.ClapTrap::name;
	this->_name = rhs.name;
	return *this;
}

void	DiamondTrap::attack(const std::string &target)
{
	ScavTrap::attack(target);
}

void	DiamondTrap::whoAmI( void ) const
{
	std::cout << "I am " << this->_name << " and my ClapTrap name is "
		<< ClapTrap::name << std::endl;
}
