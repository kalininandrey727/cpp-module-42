#include "ClapTrap.hpp"

ClapTrap::ClapTrap(const std::string name_) : name(name_), health(10), energy(10), max_health(10), damage(0)
{
	std::cout << "ClapTrap " << name << " created!" << std::endl;
}

ClapTrap::ClapTrap() : name(""), health(10), energy(10), max_health(10), damage(0)
{
	std::cout << "Default ClapTrap created!" << std::endl;
}

ClapTrap::~ClapTrap()
{
	std::cout << "ClapTrap " << name << " destroyed!" << std::endl;
}

void ClapTrap::attack(const std::string& target)
{
	if (IsWorking())
	{
		--energy;
		std::cout << "ClapTrap " << name << " attacks " << target << ", causing " << damage << " points of damage!" << std::endl;
		if (energy == 0)
			std::cout << "ClapTrap " << name << " ran out of energy!" << std::endl;
	}
}

void ClapTrap::takeDamage(unsigned int amount)
{
	if (health > 0)
	{
		health -= amount;
		std::cout << "ClapTrap " << name << " takes " << amount << " points of damage!" << std::endl;
	}
	if (health <= 0)
		std::cout << "ClapTrap " << name << " is broken!" << std::endl;
}

void ClapTrap::beRepaired(unsigned int amount)
{
	if (health == max_health)
	{
		std::cout << "ClapTrap " << name << " is already fully repaired!" << std::endl;
	}
	else if (IsWorking())
	{
		--energy;
		health += amount;
		std::cout << "ClapTrap " << name << " repaires himself by " << amount << " hit points!" << std::endl;
		if (health >= max_health)
		{
			health = max_health;
			std::cout << "ClapTrap " << name << " is now fully repaired!" << std::endl;
		}
		if (energy == 0)
			std::cout << "ClapTrap " << name << " ran out of energy!" << std::endl;
	}
}

bool ClapTrap::IsWorking() const
{
	return (energy && health > 0);
}

int ClapTrap::getHealth() const
{
	return health;
}

int ClapTrap::getEnergy() const
{
	return energy;
}