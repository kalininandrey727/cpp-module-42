#include "FragTrap.hpp"

FragTrap::FragTrap(std::string name_) : ClapTrap(name_)
{
	this->max_health = 100;
	this->energy = 100;
	this->damage = 30;
	this->health = max_health;
	std::cout << "FragTrap " << name << " created!" << std::endl;
}

FragTrap::FragTrap()
{
	this->max_health = 100;
	this->energy = 100;
	this->damage = 30;
	this->health = max_health;
	std::cout << "FragTrap " << name << " created!" << std::endl;
}

FragTrap::~FragTrap()
{
	std::cout << "FragTrap " << name << " destroyed!" << std::endl;
}

void FragTrap::highFivesGuys()
{
	if (IsWorking())
		std::cout << "FragTrap " << name << ": Guys! What\'s about high five?! ...Guys?" << std::endl;

}