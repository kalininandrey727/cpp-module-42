#pragma once

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class DiamondTrap: public FragTrap, public ScavTrap
{
private:
	std::string	_name;
	DiamondTrap(void);

public:
	DiamondTrap(const std::string &name);
	DiamondTrap(const DiamondTrap &src);
	~DiamondTrap(void);

	DiamondTrap	&operator=(const DiamondTrap &other);

	void	attack(const std::string &target);
	void	whoAmI(void) const;
};