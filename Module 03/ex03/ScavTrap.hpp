#pragma once
#include "ClapTrap.hpp"

class ScavTrap : virtual public ClapTrap
{
public:
	ScavTrap(std::string name_);
	~ScavTrap();
	void guardGate();

protected:
	ScavTrap();
};