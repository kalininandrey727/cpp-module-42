#pragma once
#include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap
{
public:
	FragTrap(std::string name_);
	~FragTrap();
	void highFivesGuys();

protected:
	FragTrap();
};