#include "ClapTrap.hpp"
#include <cstdlib>

int main( void )
{
	ClapTrap ct("Bubi");
	std::srand(72727);
	while (ct.IsWorking())
	{
		ct.attack("demon");
		ct.takeDamage(std::rand() % 10);
		ct.beRepaired(std::rand() % 10);
		std::cout << "Current energy points: " << ct.getEnergy() << ", health points: " << ct.getHealth() << std::endl;
	}
	return 0;
}