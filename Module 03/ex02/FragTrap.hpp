#pragma once
#include "ClapTrap.hpp"

class FragTrap : public ClapTrap
{
public:
	FragTrap(std::string name_);
	~FragTrap();
	void highFivesGuys();
};