#include "FragTrap.hpp"
#include <cstdlib>

int main( void )
{
	FragTrap ft("Bubi");
	std::srand(72727);
	while (ft.IsWorking())
	{
		ft.attack("demon");
		ft.takeDamage(std::rand() % 25);
		ft.beRepaired(std::rand() % 25);
		std::cout << "Current energy points: " << ft.getEnergy() << ", health points: " << ft.getHealth() << std::endl;
	}
	return 0;
}